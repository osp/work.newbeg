import math
import os, os.path
import random
import utils
import string
import settings
import datetime

from flask import Flask, send_file, render_template, request, abort

skeletons = {
    'HersheySans1': os.path.abspath('fonts/skeletons/HersheySans1.svg'),
    'HersheySerifMed': os.path.abspath('fonts/skeletons/HersheySerifMed.svg')
}

app = Flask(__name__)

@app.route('/font', methods=["GET"])
def font ():
  outputDir = os.path.abspath(os.path.join("fonts", "generated"))

  skeleton = request.args['skeleton']
  skeletonPath = skeletons[skeleton]

  scaleX = float(request.args['scaleX'])
  scaleY = float(request.args['scaleY'])
  skewX = math.tan(math.radians(min(float(request.args['skew']), 70)))
  pen = request.args['pen'] # https://fontforge.org/docs/scripting/python/fontforge.html#fontforge.glyph.stroke
  penWidth = max(10, int(request.args['penWidth']))
  penMinorWidth = max(10, int(request.args['penMinorWidth']))
  penAngle = math.radians(int(request.args['penAngle']))

  fontname = "{}-{:.2f}-{:.2f}-{:.2f}-{}-{}-{}-{}.otf".format(skeleton, scaleX, scaleY, skewX, pen, penWidth, penMinorWidth, penAngle)
  fontPath = os.path.join(outputDir, fontname)

  try:
    utils.makeFont(skeletonPath, fontPath, pen, penWidth, penMinorWidth, penAngle, scaleX, scaleY, skewX)
  except:
    return abort(500, 'Error:\n{}'.format(e.output.decode()))

  return send_file(fontPath)



@app.route('/')
def index ():
  context = { 'skeletons': skeletons }
  return render_template('index.html', **context)


@app.route('/author')
def make_author ():
  author_name = ''.join([ random.choice(string.ascii_letters) for _ in range(10) ])
  author_fonts = [('{}-{}'.format(author_name, random.randint(0,1000)), utils.get_font_for_author_at_time(author_name, t)) for t in [
    datetime.datetime(2023, 6, 25, 12, random.randint(0, 5), 0, 0),
    datetime.datetime(2023, 6, 25, 12, random.randint(10, 20), 0, 0),
    datetime.datetime(2023, 6, 25, 12, random.randint(25, 35), 0, 0),
    datetime.datetime(2023, 6, 25, 12, random.randint(40, 50), 0, 0)
  ]]

  return render_template('proof.html', fonts=author_fonts)

@app.route('/proof')
def proof ():
  scaleX = [0.7, 0.85, 1, 1.15, 1.3]
  skew = [math.radians(-5), math.radians(-2.5), math.radians(0), math.radians(2.5), math.radians(5)]
  penWidth = [60, 80, 100, 120, 140]
  # penAngle = [math.radians(0), math.radians(20), math.radians(40), math.radians(60), math.radians(80), math.radians(100), math.radians(120), math.radians(140), math.radians(160)]

  penAngle = 0

  fontCount = 4
  fonts = []

  for x in range(0, fontCount):
    skeleton = 'HersheySans1'
    
    scaleXPick = random.choice(scaleX)
    skewXPick = random.choice(skew)
    penWidthPick = random.choice(penWidth)

    for a in range(0, 4):
      # penAngle -= 2
      penAngle -= 15 #random.randint(5,10)
      fontPath = os.path.join('static', '{}-{}.otf'.format(x, a))

      fonts.append((x*10+a, fontPath))
    
      utils.makeFont(
        skeletons[skeleton],
        fontPath,
        scaleX = scaleXPick,
        skewX = skewXPick,
        penWidth = penWidthPick,
        penAngle = math.radians(penAngle))
        # penAngle = random.choice(penAngle))
      
      # penWidthPick += 5

  return render_template('proof.html', fonts=fonts)

@app.route('/logo')
def logo():
  scaleX = [0.7, 0.85, 1, 1.15, 1.3]
  skew = [-5, -2.5, 0, 2.5, 5]
  penWidth = [40, 60, 80, 100, 120, 140]
  penAngle = [0, 20, 40, 60, 80, 100, 120, 140, 160]

  fonts = []

  skeleton = 'HersheySans1'  
  scaleXPick = random.choice(scaleX)
  skewXPick = random.choice(skew)
  penWidthPick = random.choice(penWidth)
  penAnglePick = random.choice(penAngle)

  # scaleXPick = 0.5
  penAnglePick = 0
  penWidthPick = 100
  # skewXPick = 0
  
  logo = "New Beginnings"
  x = 1 # ???


  for a in range(0, len(logo)):
    fontPath = os.path.join('static', '{}-{}.otf'.format(x, a))
    fonts.append((x*10+a, fontPath))

    utils.makeFont(
        skeletons[skeleton],
        fontPath,
        scaleX = scaleXPick,
        skewX = math.radians(round(skewXPick, 2)),
        penWidth = penWidthPick,
        penAngle = math.radians(penAnglePick)
    )

    penAnglePick -= (180 / len(logo))
    # scaleXPick += ((1.5-0.5) / len(logo))
    # penWidthPick += int((160 - 20) / len(logo))
    # skewXPick += ((15) / len(logo))
 
  return render_template('logo.html', fonts=fonts, word="New Beginnings")
