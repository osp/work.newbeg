import os, os.path
import fontforge
import psMat
import argparse

"""
  Transforms the font
  font FontForge font object
  scaleX number scaling on x-axis
  scaleY number scaling on y-axis
  skewX number skew of font, in radians
"""
def transformFont (font, scaleX=1, scaleY=1, skew=0):
  for glyph in font.glyphs():
    glyph.transform(psMat.skew(skew))
    glyph.transform(psMat.scale(scaleX, scaleY))

def strokeFont (font, pen, penWidth, penMinorWidth, penAngle):
  for glyph in font.glyphs():
    if glyph.glyphname in [ "six", "eight", "nine"]:
      # Avoid part of the glyph to be filled
      glyph.stroke(pen, penWidth, penMinorWidth, cap="nib", join="nib", angle=penAngle, removeoverlap="contour")
    else:
      glyph.stroke(pen, penWidth, penMinorWidth, cap="nib", join="nib", angle=penAngle)
    glyph.correctDirection()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(
    prog='Skeleton outliner',
    description='Outlines a skeleton.'
  )

  parser.add_argument('filename_skeleton')
  parser.add_argument('filename_font', help='Path to store the generated font at')
  parser.add_argument('-p', '--pen', dest='pen', choices=['elliptical', 'calligraphic'], default='elliptical', help='Shape of the pen.')
  parser.add_argument('-w', '--pen-width', dest='penWidth', default=50,  type=int, help='Width of the pen.')
  parser.add_argument('-W', '--pen-minor-width', dest='penMinorWidth', default=50, type=int, help='Width of pen on minor axis. Depends pen type.')
  parser.add_argument('-a', '--pen-angle', dest='penAngle', default=0.0, type=float, help='Rotation of the pen.')
  parser.add_argument('-s', '--scale-x', dest='scaleX', default=1.0, type=float, help='Scaling on x-axis.')
  parser.add_argument('-S', '--scale-y', dest='scaleY', default=1.0, type=float, help='Scaling on y-axis.')
  parser.add_argument('-r', '--skew', dest='skew', default=0.0, type=float, help='Skew of the glyphs, in radians.')
  args = parser.parse_args()

  font = fontforge.open(args.filename_skeleton)
  
  transformFont(font, args.scaleX, args.scaleY, args.skew)
  strokeFont(font, args.pen, args.penWidth, args.penMinorWidth, args.penAngle)

  if not os.path.exists(os.path.dirname(args.filename_font)):
    os.makedirs(os.path.dirname(args.filename_font))
  
  font.generate(args.filename_font)
