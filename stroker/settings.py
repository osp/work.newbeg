import os.path
import datetime

VARIATION_OPTIONS = {
  'scaleX': [0.7, 0.85, 1, 1.15, 1.3],
  'skewX': [-5, -2.5, 0, 2.5, 5],
  'penWidth': [60, 80, 100, 120, 140]
}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

FONT_DIR = os.path.join(BASE_DIR, 'static', 'fonts')

FONT_PATH_PUBLIC = '/static/fonts'

REGISTRY_PATH = os.path.join(BASE_DIR, 'registry.json')

STROKE_SCRIPT_PATH = os.path.join(BASE_DIR, 'stroke-skeleton.py')

SKELETON_PATH = os.path.join(BASE_DIR, 'fonts', 'skeletons', 'HersheySans1.svg')

FONT_EPOCH = datetime.datetime(2023, 6, 25, 12, 0, 0, 0)