import os.path
import settings
import json
import subprocess
import tempfile
import random
import math

# {
#   available_combinations: [
#     {
#       'scaleX': float
#       'skew': int
#       'penWidth': int
#     }, ...
#   ]
#   combination_index: {
#     'str:identifier': {
#       'scaleX': float
#       'skew': int
#       'penWidth': int
#     }
#   }
# }

def make_combinations (key, options, exisiting_combinations):
  new_combinations = []

  for existing_option in exisiting_combinations:
    for new_option in options:

      new_combinations.append({
        **existing_option,
        key: new_option
      })

  return new_combinations


def make_all_combinations (**variations):
  combinations = []

  for (variation_key, options) in variations.items():
    if combinations:
      combinations = make_combinations(variation_key, options, combinations)
    else:
      combinations = [ { variation_key: option } for option in options ]

  return combinations


def init_registry ():
  store_registry({
    'available_combinations': make_all_combinations(**settings.VARIATION_OPTIONS),
    'combination_index': {}
  })
  

def store_registry (registry):
  with open(settings.REGISTRY_PATH, 'w') as h:
    json.dump(registry, h, ensure_ascii=False)


def parseJsonFile(path):
  with open(path, "r") as jsonfile:
    data = json.load(jsonfile)
    return data
  

def load_registry ():
  if not os.path.exists(settings.REGISTRY_PATH):
    init_registry()

  return parseJsonFile(settings.REGISTRY_PATH)


def makeFont(skeletonPath, outputPath, pen='elliptical', penWidth=20, penMinorWidth=20, penAngle=0, scaleX=1, scaleY=1, skewX=0, debug=False):
  args = [
    'python3',
    settings.STROKE_SCRIPT_PATH,
    '--pen', pen,
    '--pen-width', str(penWidth),
    '--pen-minor-width', str(penMinorWidth),
    '--pen-angle', str(penAngle),
    '--scale-x', str(scaleX),
    '--scale-y', str(scaleY),
    '--skew', str(skewX),
    skeletonPath,
    outputPath
  ]

  if debug:
    print(' '.join(args))

  subprocess.check_output(args, cwd=os.path.dirname(os.path.abspath(__file__)), stderr=subprocess.STDOUT, timeout=5).decode()

  return True

def test_combination (combination, skeleton):
  with tempfile.TemporaryDirectory(prefix='newbeg__') as tmpdir:
    for penAngle in [0, 15, 30, 45, 60]:
      try:
        makeFont(
          skeleton,
          os.path.join(tmpdir, 'font.otf'),
          scaleX = combination['scaleX'],
          skewX = math.radians(combination['skewX']),
          penWidth = combination['penWidth'],
          penAngle = math.radians(penAngle),
          debug=True
        )
      except:
        return False

  # tmpdir.cleanup()    
  return True

def pick_and_test_combination (registry, skeleton):
  if len(registry['available_combinations']) > 0:
    pick = random.randint(0, len(registry['available_combinations']) - 1)
    combination = registry['available_combinations'][pick]
    registry['available_combinations'] = registry['available_combinations'][:pick] + registry['available_combinations'][pick:]
  
    if test_combination(combination, skeleton):
      return (combination, registry)
    else:
      print("Dropped", combination)
      return pick_and_test_combination(registry, skeleton)    

  else:
    print('Ran out of options!')
    return(None, registry)


def get_combination_for_author(author_name):
  registry = load_registry()
  if author_name in registry['combination_index']:
    combination = registry['combination_index'][author_name]
  else:
    (combination, registry) = pick_and_test_combination(registry, settings.SKELETON_PATH)
    registry['combination_index'][author_name] = combination
    store_registry(registry)
  
  return combination

def get_font_for_author_at_time (author_name, time):
  penAngle = math.floor((time - settings.FONT_EPOCH).seconds / 60) % 360
  print((time - settings.FONT_EPOCH).seconds, penAngle)
  fontName = '{}-{}.otf'.format(author_name, penAngle)
  fontPath = os.path.join(settings.FONT_DIR, fontName)

  if not os.path.exists(fontPath):
    combination = get_combination_for_author(author_name)
    makeFont(
      settings.SKELETON_PATH,
      fontPath,
      scaleX = combination['scaleX'],
      skewX = math.radians(combination['skewX']),
      penWidth = combination['penWidth'],
      penAngle = math.radians(penAngle)
    )

  return os.path.join(settings.FONT_PATH_PUBLIC, fontName)