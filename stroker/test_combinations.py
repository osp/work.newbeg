import utils
import settings

failed = []
passed = []

combinations = utils.make_all_combinations(**settings.VARIATION_OPTIONS)

for combination in combinations:
  if utils.test_combination(combination, settings.SKELETON_PATH):
    passed.append(combination)
  else:
    failed.append(combination)

print("Tested {} combinations".format(len(combinations)))
print("Valid combinations: {}".format(len(passed)))
print("Failing combinations: {}".format(len(failed)))