import json
import time
import random


from faker import Faker
from nanoid import generate

def generate_text():
    text = ""
    for i in range(random.randrange(5,20)):
        text += Faker().paragraph()
    return  text


def generate_round(author_id, round_id, corpus_id, rnd_type, time):
    round_data = {"round_id": round_id, 
                  "corpus_id": corpus_id, 
                  "author_id": author_id,
                  "start_time": time,
                  "end_time": time + 900,
                  "rnd_type": rnd_type,
                  "prev_id": None,
                  "prompt": "",
                  "text": generate_text(), 
                  "input": "", 
                  "complete": True}
    return round_data

def generate_corpus():
    current_time = time.time()
    corpus_id = generate()
    round_types = ['compose_prompt', 'plain', 'edit_markov', 'prompted' ]
    authors = [Faker().email() for author in range(48)]
    corpus_data = {"title": "Generated corpus",
                   "rounds": [],
                   "default_prompt": "testing default prompt", 
                   "corpus_id": corpus_id
                   }
    for author in authors:
        for round in round_types:
            rnd_id = generate( size=5)
            corpus_data["rounds"].append(rnd_id)
            round_data = generate_round(author, rnd_id, corpus_id, round, current_time)
            with open(f"./output/{rnd_id}.json", "w") as jsonfile:
                json.dump(round_data, jsonfile)
            current_time += 900


    
    with open("./output/corpus.json", "w") as jsonfile:
        json.dump(corpus_data, jsonfile)

    

generate_corpus()
