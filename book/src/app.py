import json
import os
from datetime import datetime

import jinja2
import markdown

TEMPLATE = "template.html"

def get_author_fonts():
    data = os.listdir("./fonts/author_fonts/")
    stripped_data = [x.split('.')[0] for x in data] 
    return stripped_data
    


def convert_md_to_html(file):
    with open(file, "r") as md_file:
        file_contents = md_file.read()
        return markdown.markdown(file_contents)

def parse_json_file(file):
    with open(file, "r") as jsonfile:
        data = json.load(jsonfile)
        return data

def format_date(date_float): #should probably be written as a jinja2 filter
    # return datetime.utcfromtimestamp(date_float).strftime('%Y-%m-%d %H:%M:%S')
    return datetime.utcfromtimestamp(date_float).strftime('%H:%M')

def format_round(round_dict):
    formatted_round = {
        "round_id": round_dict['round_id'],
        "corpus_id": round_dict['corpus_id'],
        "author_id": round_dict['author_id'],
        "start_time": format_date(round_dict['start_time']),
        "end_time": format_date(round_dict['end_time']),
        "rnd_type": round_dict['rnd_type'],
        "prev_id": round_dict['prev_id'],
        "prompt": round_dict['prompt'],
        "input": round_dict['input'],
        "complete": round_dict['complete'],
        "text": [x for x in round_dict['text'].split('\n') if len(x) > 0]
    }
    return formatted_round

def generate_html_file(data, paratext):
    templateLoader = jinja2.FileSystemLoader( searchpath="./templates/" )
    env = jinja2.Environment(loader=templateLoader)

    template = env.get_template("book_template.html")
 
    html = template.render(rounds_data = data, paratext=paratext)
    with open('book.html', 'w') as file:
        file.write(html)

def generate_front_cover(authors):
    templateLoader = jinja2.FileSystemLoader( searchpath="./templates/" )
    env = jinja2.Environment(loader=templateLoader)

    template = env.get_template("front_cover_template.html")

    html = template.render(authors = authors)
    with open('front_cover.html', 'w') as file:
        file.write(html)

def generate_back_cover():
    templateLoader = jinja2.FileSystemLoader( searchpath="./templates/" )
    env = jinja2.Environment(loader=templateLoader)

    template = env.get_template("back_cover_template.html")

    html = template.render(authors = authors)
    with open('back_cover.html', 'w') as file:
        file.write(html)

corpus = parse_json_file("../data/corpus.json")
rounds_data = [format_round(parse_json_file(f"../data/{round}.json")) for round in corpus["rounds"]]
paratext = convert_md_to_html("../data/paratext.md")

generate_html_file(rounds_data, paratext)
generate_front_cover(get_author_fonts())






