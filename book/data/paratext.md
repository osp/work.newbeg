#Colophon

Konzept: Andreas Bülhoff, Jenny Bohn, Brendan Howell

nach einem Konzept und Programm von Brendan Howell

Design(?): OSP

NEW BEGINNINGS Schreibperformance | 30.6.-2.7. | Haus Rüschhaus, Münster / Digitale Burg

Burg Hülshoff – Center for Literature

\*NEW BEGINNINGS (stolen by GIESCHE, who also stole it from someone else)

NEW BEGINNINGS wird gefördert im Rahmen von »Neustart Kultur« der Beauftragten der Bundesregierung für Kultur und Medien durch den Deutschen Literaturfonds e.V.

#Participating authors

Hatice Açıkgöz, Duygu Ağal, Elisa Aseva, Hannes Bajohr, Josefine Berkholz, Donat Blum, Kaśka Bryla, Andreas Bülhoff, Susi Bumms, Dorothee Elmiger, Jan Erbelding, Daniel Falb, Lars Fleischmann, Grashina Gabelmann, Heike Geißler, Berit Glanz, Uwe Huth, Dagmara Kraus, Tim Holland, Suse Itzel, Mascha Jacobs, Stephan Janitzky, Thorsten Krämer, Dagmara Kraus, Anja Kümmel, Son Lewandowski, Enis Maci, Ilija Matusko, Nele Müller, Biba Oskar Nass, Jennifer de Negri, Rudi Nuss, Hendrik Otremba, Kathrin Passig, Pascal Richmann, Monika Rinck, Tanasgol Sabbagh, Fabian Saul, Simone Scharbert, Rike Scheffler, Hannah Schraven, Eva Tepest, Kinga Tóth, Senthuran Varatharajah, Sinthujan Varatharajah, Stefanie de Velasco, Maxi Wallenhorst, Sarah Claire Wray
