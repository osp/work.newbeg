# Changelog

Alpha-Version  
Münster, 30. Juni 2023, 15:00 – 2. Juli 2023, 14:00  
Stündliches Neuordnen des Textes auf Basis aller bisher eingegangenen Texte

Beta-Version  
Münster, 2. Juli 2023, 15:04  
Randomisierung gestoppt, stabiler Text

Stable Release  
Berlin, 10. Juli 2023, <span id="time-now"></span>  
Nachwort hinzugefügt, Changelog hinzugefügt, leichte Korrekturen im Kolophon, kein Eingriff in den Text


<script>
  let d = new Date()
  
  document.getElementById('time-now').innerHTML = `${ d.getHours() }:${ (d.getMinutes() < 10) ? '0' : '' }${ d.getMinutes() }`
</script>

