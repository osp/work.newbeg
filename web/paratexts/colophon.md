# Kolophon

New Beginnings

eine algorithmisch editierte Booksprint-Schreibperformance

30.6.–2.7., Haus Rüschhaus, Münster / Digitale Burg

Konzept: Andreas Bülhoff, Jennie Bohn, Brendan Howell

Nach dem Programm The Maggot von Brendan Howell

Mit Texten von Hatice Açıkgöz, Duygu Ağal, Elisa Aseva, Hannes Bajohr, Josefine Berkholz, Donat Blum, Kaśka Bryla, Andreas Bülhoff, Susi Bumms, Dorothee Elmiger, Jan Erbelding, Daniel Falb, Lars Fleischmann, Grashina Gabelmann, Heike Geißler, Mara Genschel, Berit Glanz, Uwe Huth, Tim Holland, Suse Itzel, Mascha Jacobs, Stephan Janitzky, Thorsten Krämer, Dagmara Kraus, Anja Kümmel, Son Lewandowski, Enis Msnapshotaci, Ilija Matusko, Nele Müller, Biba Oskar Nass, Jennifer de Negri, Rudi Nuss, Hendrik Otremba, Kathrin Passig, Pascal Richmann, Monika Rinck, Fabian Saul, Simone Scharbert, Rike Scheffler, Hannah Schraven, Karosh Taha, Evan Tepest, Kinga Tóth, Senthuran Varatharajah, சிந்துஜன் வரதராஜா (Sinthujan Varatharajah), Stefanie de Velasco, Maxi Wallenhorst, Sarah Claire Wray

Gestaltung: Gijs de Heij und Einar Andersen von Open Source Publishing

Diese Publikation ist lizenziert unter CC BY-NC-SA 4.0.

Die Editionssoftware ist lizensiert unter GNU GPL-3.

Der Sourcecode für die Software ist auf https://git.sr.ht/~bhowell/the-maggot/tree/maggot2 verfügbar.

Die generativen Schriften für diese Publikation sind lizenziert unter CC4r.

Der Sourcecode für diese Publikation ist auf https://gitlab.constantvzw.org/osp/work.newbeg verfügbar.

The title “New Beginnings” was stolen from GIESCHE, who also stole it from someone else.

<div class="sponsor-logo-container">
  <img src="static/logos/CDR_BKM_Neustart_Kultur_Wortmarke_pos_RGB_RZ.svg" alt="Neustart Kultur Logo" class="sponsor-logo" />
</div>

NEW BEGINNINGS wird gefördert im Rahmen von »Neustart Kultur« der Beauftragten der Bundesregierung für Kultur und Medien durch den Deutschen Literaturfonds e.V.

<div class="sponsor-logo-container">
  <img src="static/logos/CFL_LOGO_AVD_Stiftung-kompakt.jpg" alt="Annette von Droste zu Hülshoff STIFTUNG Logo" class="sponsor-logo" />
  <img src="static/logos/CFL_Logo-lang_quer_BH_medium.jpg" alt="Center for Literature / Burg Hülshoff Logo" class="sponsor-logo" />
</div>

Die Annette von Droste zu Hülshoff-Stiftung ist Trägerin von Burg Hülshoff, Haus Rüschhaus und dem Center for Literature (CfL). Die Stiftung wird gefördert durch den Landschaftsverband Westfalen-Lippe, das Ministerium für Kultur und Wissenschaft des Landes Nordrhein-Westfalen sowie die Beauftragte der Bundesregierung für Kultur und Medien.

<div class="sponsor-logo-container">
  <img src="static/logos/LWL-Logo_ohne_Claim_schwarz_RZ.png" alt="LWL Logo" class="sponsor-logo" />
  <img src="static/logos/Ministerium_Kultur_und_WIssenschaft_NRW_Logo_DE_farbig.jpg" alt="Ministerium für Kultur und WIssenschaft des Landes NRW logo" class="sponsor-logo" />
  <img src="static/logos/BKM_2017_WebSVG_de.svg" alt="Logo Die Beauftragte der Bundesregierung für Kultur und Medien" class="sponsor-logo" />
</div>

Münster 2023
