# Never do the actual thing (Nachwort)

Ich öffne eine Website, auf ihr erscheint ein Timer und darunter ein Textfeld. Der Timer zeigt eine Zeit, sagen wir 5 Minuten. Das Textfeld ist leer und wartet auf eine Eingabe. Die Zeit läuft ab.

Wenn der Timer bei 0 ist, verschwindet der Text und es beginnt etwas Neues. Der Timer ist bei 0, mein Text verschwindet. Jetzt zeigt er 15 Minuten und läuft rückwärts. Das Textfeld ist leer und ich beginne von Neuem mit der Eingabe.

Wenn der Timer bei 0 ist, wird der Text in einer Datei auf einem Server gespeichert und mit Metadaten versehen. 7 Dateien entstehen so pro Autor\*in in einer einstündigen Session, die in kleinere, verschieden lange Blöcke getaktet ist. 48 Autor\*innen schreiben für je eine Stunde, das macht 336 gespeicherte und mit Metadaten versehene Dateien. Aus diesen Dateien formt ein Script nach bestimmten Regeln und Zufallsfaktoren automatisch und in real-time den hier stillgestellten Text.

Die Wahrheit ist: Wenige Dateien sind unwiederbringlich verloren gegangen. Die Wahrheit ist: Wir haben eine Completion Rate von etwa 98,5%. Jede der 48 Autor\*innen hat geschrieben, es gab keine Ausfälle. Das heißt, jede der 48 Autor\*innen ist mit Text im Buch vertreten. Aber hin und wieder sickerte eben etwas durch die Software, leckte durch die Kabel oder den Funk, wurde nicht abgerufen oder übertragen. Ok.

Der Text dieses Buches wurde also kollektiv geschrieben. Kollektiv geschrieben heißt in diesem Fall: Zwar schreibe ich einzelne, aber durch die spezielle Beschaffenheit des Interface, in das ich schreibe, geht jede Eingabe unwiederbringlich in die kollektive, randomisierte Ordnung dieses Buches ein. Jede Eingabe kann nicht anders als sich in neue, unbekannte Anschlüsse zu fügen: Die Gier des kollektiven Dokuments nach dem individuellen Input.

All das ist den algorithmisch sortierten Timelines von Social Media gar nicht unähnlich. Nur dass es in dieser Abfolge keine Namen oder Profilbilder mehr gibt, die Autor\*innenschaft also nicht mehr an einzelne Aussagen knüpfbar ist. Trotzdem sollte der Text, den das Programm prozessiert, einzelne Stimmen nicht negieren, sondern Text im Sinne von Allmende als einen kollektiven, vielstimmigen Ort inszenieren, an dem sich Einzelnes und Gemeinsames gleichermaßen ablesen lässt. Ein Arretieren also zwischen Subtext und Textsuppe.

Algorithmisch bedeutet das: Jede Eingabe bleibt in der Verarbeitung weitestgehend intakt. Grafisch bedeutet das: Die Eingaben einer Person werden im Buch in der gleichen Schrift ausgezeichnet. Dieses von Gijs de Heij und Einar Andersen (Open Source Publishing) entwickelte Konzept nutzt eine Basisschrift, von der nach Abschluss jeder Schreibsession automatisch eine einzigartige Variante generiert wird, in der das Geschriebene nun dargestellt ist. Alle Abschnitte gehen also auf die gleiche, im Paratext dieses Buches verwendete Schriftart zurück und sind zugleich einzigartige Ausformungen von dieser.

Was dabei entstanden ist, ist ebenso wenig Korpusliteratur wie wildes Cut-Up, sondern irgendwas dazwischen. Denn trotz Randomisierung und der Vermeidung algorithmischer Analysen der Semantik wurden eine Reihe kohärenzstiftender Regime programmiert. Zum Beispiel: Ich bekomme die Aufgabe gestellt, eine Überschrift zu formulieren. Im Buch erscheinen dann alle Überschriften chronologisch und strukturieren den Text in 48 Segmente. Zum Beispiel: Ich bekomme eine Aufgabe als Schreibimpuls, die mir von einer Person, die vor mir schrieb, gestellt wurde, und muss später ebenfalls eine Aufgabe für eine nach mir schreibende Person formulieren – eine Art Stille Post-Prinzip. Zum Beispiel: Alle geschriebenen Texte gehen in einen Korpus ein, aus dem mir per Markow-Kette ein Textkonglomerate einer bestimmten Länge angezeigt wird, in das ich korrigierend eingreifen kann.

New Beginnings ist ebenso eine Übung in Scripted Design wie ein Experiment in Scripted Editing und domestiziertem Schreiben. Es wird ein Buch gestalterisch und in der Abfolge entworfen, für das noch keine Inhalte feststehen. Wo der Input dem Takt der Maschine folgt, folgt der Output dem Arrangement der Maschine. Entscheidend ist dabei zunächst, wer das Interface designed (wir), wer das Buch designed (OSP), wer den Inhalt liefert (48 Autor\*innen) und wer bestimmt, wie diese Inhalte arrangiert werden (ebenfalls wir). Aha. Klingt fast so, als ginge es gar nicht um den Text.

Wenn der Timer bei 0 ist, läuft also alles automatisch. Der Timer ist bei 0 heißt, der geschriebene Text bekommt seinen Platz in der halbchaotischen Ordnung des Buches, die so chaotisch im semantischen Geflecht dann doch nicht mehr ist. Jetzt beginnt das Lesen immer von Neuem.

Berlin, 4. Juli 2023


<img src="static/nachtwort_images/flowchart_left.png" alt="New Beginnings flowchart left" class="flowchart-l" />

<p class="flowchart-caption">newbeg-error_msg_flowchart.pdf</p>

<img src="static/nachtwort_images/flowchart_right.png" alt="New Beginnings flowchart right" class="flowchart-r"/>


<img src="static/img/screenshot.png" alt="New Beginnings interface Screenshot" class="image-full"/>

<p class="break-after">newbeg-writing_interface_screenshot.png</p>


# Die Schreibaufgaben:

1. Schreibe eine Antwort auf einen Text, den dir eine vorhergehende Person hinterlassen hat, Zeit: 14 Minuten
2. Freies Schreiben 1/3, Zeit: 5 Minuten
3. Freies Schreiben 2/3, Zeit: 10 Minuten
4. Freies Schreiben 3/3, Zeit: 7 Minuten
5. Schreibe eine Aufgabe für eine Person, die nach dir schreibt, Zeit: 5 Minuten
6. Korpuspflege: 200 Wörter, die per Markov-Algorithmus mit n=3 Wörtern aus allen bisherigen Texten generiert wurden und die bearbeitet oder gelöscht werden können, Zeit: 15 Minuten
7. Formuliere eine Kapitelüberschrift, Zeit: 3 Minuten


&nbsp;  


# Die Anordnung im Buch:

- Beginne auf einer Recto-Seite
- Setze in chronologischer Reihenfolge eine noch nicht verwendete Kapitelüberschrift
- setze 5 zufällige, noch nicht verwendete Chunks (ausgenommen Überschrift und Schreibaufgabe), bei denen es sehr unwahrscheinlich ist, dass zwei der gleichen Autor\*in aufeinanderfolgen
- Wiederhole, bis alle 48 Kapitel abgearbeitet sind
- Ist einer der geforderten Faktoren nicht vollständig (beispielsweise, weil der Überschriften-Input nicht richtig übertragen wurde), überspringe den Schritt
- Liste alle 48 Schreibaufgaben in chronologischer Reihenfolge als Appendix
