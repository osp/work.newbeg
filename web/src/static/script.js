const toggleRound = (event, roundId) => {
  document
    .querySelectorAll(".round")
    .forEach((round) => round.classList.remove("active"));
  document
    .querySelectorAll(".text-menu-button")
    .forEach((button) => button.classList.remove("active"));
  roundId.classList.add("active");
  event.target.classList.add("active");
  console.log(event, roundId);
};
