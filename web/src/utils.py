import os.path
import settings
import json
import subprocess
import tempfile
import random
import math
import datetime
import markdown
import glob
import itertools

"""
Registry.json:

{
  'available_combinations': [
    {
      'scaleX': float
      'skew': int
      'penWidth': int
    }, ...
  ]
  'combination_index': {
    'str:author_id': {
      'scaleX': float
      'skew': int
      'penWidth': int
    }
  },
  'completed_authors': [
    str:author_id,
    ...
  ]
}

"""


def make_combinations (key, options, exisiting_combinations):
  new_combinations = []

  for existing_option in exisiting_combinations:
    for new_option in options:

      new_combinations.append({
        **existing_option,
        key: new_option
      })

  return new_combinations


def make_all_combinations (**variations):
  combinations = []

  for (variation_key, options) in variations.items():
    if combinations:
      combinations = make_combinations(variation_key, options, combinations)
    else:
      combinations = [ { variation_key: option } for option in options ]

  return combinations


def init_registry ():
  store_registry({
    'available_combinations': make_all_combinations(**settings.VARIATION_OPTIONS),
    'combination_index': {}
  })
  

def store_registry (registry):
  with open(settings.REGISTRY_PATH, 'w') as h:
    json.dump(registry, h, ensure_ascii=False)


def parseJsonFile(path):
  with open(path, "r") as jsonfile:
    data = json.load(jsonfile)
    return data
  

def load_registry ():
  if not os.path.exists(settings.REGISTRY_PATH):
    init_registry()

  return parseJsonFile(settings.REGISTRY_PATH)


def makeFont(skeletonPath, outputPath, pen='elliptical', penWidth=20, penMinorWidth=20, penAngle=0, scaleX=1, scaleY=1, skewX=0, debug=False):
  args = [
    settings.STROKE_SCRIPT_PYTHON_EXECUTEABLE,
    settings.STROKE_SCRIPT_PATH,
    '--pen', pen,
    '--pen-width', str(penWidth),
    '--pen-minor-width', str(penMinorWidth),
    '--pen-angle', str(penAngle),
    '--scale-x', str(scaleX),
    '--scale-y', str(scaleY),
    '--skew', str(skewX),
    skeletonPath,
    outputPath
  ]

  if debug:
    print(' '.join(args))
  try:
    subprocess.check_output(args, cwd=os.path.dirname(os.path.abspath(__file__)), stderr=subprocess.STDOUT, timeout=5).decode()
  except:
    return False
  return True

def test_combination (combination, skeleton):
  with tempfile.TemporaryDirectory(prefix='newbeg__') as tmpdir:
    for penAngle in [0, 15, 30, 45, 60]:
      try:
        makeFont(
          skeleton,
          os.path.join(tmpdir, 'font.otf'),
          scaleX = combination['scaleX'],
          skewX = math.radians(combination['skewX']),
          penWidth = combination['penWidth'],
          penAngle = math.radians(penAngle),
          debug=True
        )
      except:
        return False

  # tmpdir.cleanup()    
  return True

def pick_and_test_combination (registry, skeleton):
  if len(registry['available_combinations']) > 0:
    pick = random.randint(0, len(registry['available_combinations']) - 1)
    combination = registry['available_combinations'][pick]
    registry['available_combinations'] = registry['available_combinations'][:pick] + registry['available_combinations'][pick:]
  
    if test_combination(combination, skeleton):
      return (combination, registry)
    else:
      print("Dropped", combination)
      return pick_and_test_combination(registry, skeleton)    

  else:
    print('Ran out of options!')
    return(None, registry)


def get_combination_for_author(author_name):
  registry = load_registry()
  if author_name in registry['combination_index']:
    combination = registry['combination_index'][author_name]
  else:
    (combination, registry) = pick_and_test_combination(registry, settings.SKELETON_PATH)
    registry['combination_index'][author_name] = combination
    store_registry(registry)
  
  return combination


# Load complete authors from the registry
def load_complete_authors():
  registry = load_registry()

  if 'complete_authors' in registry:
    return registry['complete_authors']
  else:
    return []

# Set complete authors in the registry
def store_complete_authors(complete_authors):
  registry = load_registry()
  registry['complete_authors'] = complete_authors
  store_registry(registry)


# Add all authors to author rounds for whom the end time of
# their last round lies in the past and who have at least one
# complete round
def update_complete_authors (complete_authors, author_rounds):
  now = datetime.datetime.now()
  candidates = list(author_rounds.items())
  # Candidates is a list of tuples (author_id, rounds)
  # take end_time of last round for sorting
  candidates.sort(key=lambda candidate: candidate[1][-1]['end_time'])

  for author_id, rounds in candidates:
    if author_id not in complete_authors and rounds[-1]['end_time'] < now and any([rnd['complete'] for rnd in rounds]):
      complete_authors.append(author_id)

  return complete_authors


def hours_since (referencetime):
  now = datetime.datetime.now()
  delta = now - referencetime
  return math.floor(delta.seconds / 60 / 60)

# Return current author
# The current author is the author at index of complete author list
# Where index is the amount of hours that have past since the start
# of the project minus 1
def get_current_author (complete_authors):
  index = hours_since(settings.PROJECT_EPOCH) - 1

  if index > -1 and index < len(complete_authors):
    return complete_authors[index]
  else:
    if len(complete_authors) > 0:
      return complete_authors[-1]
    else:
      return None


def get_header_font_for_author (author_name):
  penAngle = 30
  fontName = '{}-header.otf'.format(author_name)
  fontPath = os.path.join(settings.FONT_DIR, fontName)
  if not os.path.exists(fontPath):
    combination = get_combination_for_author(author_name)
    makeFont(
      settings.SKELETON_PATH,
      fontPath,
      scaleX = combination['scaleX'],
      skewX = math.radians(combination['skewX']),
      penWidth = combination['penWidth'] + 80,
      penMinorWidth = 50,
      penAngle = math.radians(penAngle)
    )

  return os.path.join(settings.FONT_PATH_PUBLIC, fontName)

def get_font_for_author_at_time (author_name, time):
  penAngle = math.floor((time - settings.FONT_EPOCH).seconds / 60) % 360
  fontName = '{}-{}.otf'.format(author_name, penAngle)
  fontPath = os.path.join(settings.FONT_DIR, fontName)

  if not os.path.exists(fontPath):
    combination = get_combination_for_author(author_name)
    makeFont(
      settings.SKELETON_PATH,
      fontPath,
      scaleX = combination['scaleX'],
      skewX = math.radians(combination['skewX']),
      penWidth = combination['penWidth'],
      penAngle = math.radians(penAngle)
    )

  return os.path.join(settings.FONT_PATH_PUBLIC, fontName)


def parse_date(date_float):
    # should probably be written as a jinja2 filter
    # return datetime.utcfromtimestamp(date_float).strftime('%Y-%m-%d %H:%M:%S')
    return datetime.datetime.utcfromtimestamp(date_float)


def parseRoundDict(round_dict):
    parsedDict = {
      "round_id": round_dict['round_id'],
      "corpus_id": round_dict['corpus_id'],
      "author_id": round_dict['author_id'],
      "start_time": parse_date(round_dict['start_time']),
      "end_time": parse_date(round_dict['end_time']),
      "rnd_type": round_dict['rnd_type'],
      "prev_id": round_dict['prev_id'],
      "prompt": round_dict['prompt'],
      "input": round_dict['input'],
      "complete": round_dict['complete'],
      "text": [x for x in round_dict['text'].split('\n') if len(x) > 0]
    }
    return parsedDict


def parseCorpus(corpus_path):
  corpus = parseJsonFile(corpus_path)
  rounds_folder = os.path.dirname(corpus_path)
  # Load all rounds, and ensure they are sorted
  corpus['rounds'] = sorted(
    [parseRoundDict(parseJsonFile(os.path.join(rounds_folder, f"{round}.json"))) for round in corpus["rounds"]],
    key=lambda r: r['start_time']
  )

  return corpus


def getLatestComplete (roundtype, rounds):
  ## Reverse loop through round
  for r in rounds[::-1]:
    if r['complete'] and r['rnd_type'] == roundtype:
      return r
    
  return None


def getLatestCompletePerRoundType (rounds, round_types):
  return {
    k: getLatestComplete(k, rounds) for k in round_types
  }


def getCompleteRoundsPerType (rounds, round_types):
  return {
    k: list(filter(lambda r: r['rnd_type'] == k and r['complete'], rounds)) for k in round_types
  }


def getAuthors (corpus):
  return set([ round['author_id'] for round in corpus['rounds'] ])


def addFontsToRounds (rounds):
  for round in rounds:
    round['font'] = get_font_for_author_at_time(round['author_id'], round['start_time'])

  return rounds


def renderTemplate (env, template, output, **data):
  template = env.get_template(template)
  with open(output, 'w') as h:
    h.write(template.render(**data))


def convert_md_to_html(path):
  with open(path, "r") as md_file:
    file_contents = md_file.read()
    return markdown.markdown(file_contents)

"""
  Loads all copora in a folder.
  Every folder with a corpus.json inside is considered a corpus.
"""
def loadCorpora (corpora_directory):
  return [
    parseCorpus(corpus_path) for corpus_path in glob.glob(os.path.join(corpora_directory, '*/corpus.json'))
  ]


"""
  Returns a single list of rounds from multiple corpora
"""
def extractRoundsFromCorpora (corpora):
  return sorted(itertools.chain.from_iterable([ corpus['rounds'] for corpus in corpora ]), key=lambda r: r['start_time'])


def extractAuthorsFromCorpora (corpora):
  authors = {}

  for corpus in corpora:
    if 'authors' in corpus:
      for author_id, author_name in corpus['authors'].items():
        authors[author_id] = {
          'author_id': author_id,
          'author_name': author_name
        }

  return authors

"""
  Returns rounds per author
"""
def makeAuthorMapping (rounds):
  mapping = {}

  for rnd in rounds:
    if not rnd['author_id'] in mapping:
      mapping[rnd['author_id']] = [rnd]
    else:
      mapping[rnd['author_id']].append(rnd)

  return mapping


def shuffled (l):
  l_shuffled = [v for v in l]
  random.shuffle(l_shuffled)
  return l_shuffled


def shuffle_chunk_list (chunks, attempt=1):
  random.shuffle(chunks)
  
  if attempt > 5:
    # Give up
    return chunks
    
  i = 0
  chunk_count = len(chunks)

  while i < (chunk_count - 1):
    chunk = chunks[i]
    next_chunk = chunks[i+1]

    if chunk and next_chunk and chunk['author_id'] == next_chunk['author_id']:
      # Both chunk and next chunk have the same author
      # Try to reshuffle the remaining list to get a different author
      # Try at most five times

      sub_shuffle_attempt = 0
      while next_chunk and chunk['author_id'] == next_chunk['author_id'] and sub_shuffle_attempt < 5:
        sub_shuffle_attempt += 1
        chunks = chunks[:i+1] + shuffled(chunks[i+1:])
        next_chunk = chunks[i+1]

        # We try maximum of five times
        if sub_shuffle_attempt > 5:
            # Start again
            return shuffle_chunk_list(chunks, attempt+1)
        
    i += 1

  return chunks

# https://stackoverflow.com/a/312464
def chunks(lst, n):
  """Yield successive n-sized chunks from lst."""
  for i in range(0, len(lst), n):
    yield lst[i:i + n]