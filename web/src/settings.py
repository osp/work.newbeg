import os.path
import datetime

VARIATION_OPTIONS = {
  'scaleX': [0.7, 0.8, .9, 1, 1.1],
  'skewX': [-5, -2.5, 0, 2.5, 5],
  'penWidth': [60, 80, 100, 120, 140]
}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATA_DIR = os.path.join(BASE_DIR, '..', 'data')

PARATEXTS_DIR = os.path.join(BASE_DIR, '..', 'paratexts')

FONT_DIR = os.path.join(BASE_DIR, 'static', 'fonts')

REGISTRY_PATH = os.path.join(BASE_DIR, 'registry.json')

STROKE_SCRIPT_PYTHON_EXECUTEABLE = 'python3'

STROKE_SCRIPT_PATH = os.path.join(BASE_DIR, '..', '..', 'stroker', 'stroke-skeleton.py')

SKELETON_PATH = os.path.join(BASE_DIR, 'skeletons', 'HersheySans1.svg')

PROJECT_EPOCH = datetime.datetime(2023, 6, 30, 14, 0, 0, 0)

FONT_EPOCH = PROJECT_EPOCH

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')

STATIC_DIR = os.path.join(BASE_DIR, 'static')

OUTPUT_DIR = os.path.join(BASE_DIR, 'output')

STATIC_URL = 'static'

FONT_PATH_PUBLIC = os.path.join(STATIC_URL, 'fonts')

try:
  from local_settings import *
except:
  pass