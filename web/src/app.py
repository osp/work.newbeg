import jinja2
import utils
import settings
import os, os.path
import shutil
import glob
import datetime
import itertools

def collect_paratexts ():
    pattern = os.path.join(settings.PARATEXTS_DIR, '*.md')
    texts = {}

    for path in glob.glob(pattern):
        textName = os.path.splitext(os.path.basename(path))[0]
        html = utils.convert_md_to_html(path)

        texts[textName] = html
    
    return texts

def generate_html_file(rounds, authors):
    templateLoader = jinja2.FileSystemLoader(searchpath=settings.TEMPLATE_DIR)
    env = jinja2.Environment(loader=templateLoader, autoescape=True)
 
    # Add fonts to authors
    for author_id in authors:
        authors[author_id]['font'] = utils.get_font_for_author_at_time(author_id, datetime.datetime(2023,6,30,14))
        authors[author_id]['header_font'] = utils.get_header_font_for_author(author_id)
    

    round_types = set([r['rnd_type'] for r in rounds])
    rounds = utils.addFontsToRounds(rounds)
    author_mapping = utils.makeAuthorMapping(rounds)

    complete_authors = utils.update_complete_authors(utils.load_complete_authors(), author_mapping)
    utils.store_complete_authors(complete_authors)

    current_author_id = utils.get_current_author(complete_authors)
    current_author = authors[current_author_id] if current_author_id and current_author_id in authors else None

    if current_author_id:
        current_author_rounds = utils.getCompleteRoundsPerType(author_mapping[current_author_id], round_types)
    else:
        current_author_rounds = {}

    paratexts = collect_paratexts()


    rounds_per_type = utils.getCompleteRoundsPerType(rounds, round_types)
    # List of title chunk
    titles = rounds_per_type['compose_title'] if 'compose_title' in rounds_per_type else []
    # List of prompts
    prompts = rounds_per_type['compose_prompt'] if 'compose_prompt' in rounds_per_type else []
    
    # Other chunks, shuffle them
    other_chunks = []

    if 'plain' in rounds_per_type:    
        other_chunks.extend(rounds_per_type['plain'])
    if 'edit_markov' in rounds_per_type:    
        other_chunks.extend(rounds_per_type['edit_markov'])
    if 'prompted' in rounds_per_type:    
        other_chunks.extend(rounds_per_type['prompted'])

    expected_chunk_count = 5 * len(authors.keys())

    if len(other_chunks) < expected_chunk_count:
        other_chunks.extend([None for _ in range(expected_chunk_count - len(other_chunks))])

    other_chunks = utils.shuffle_chunk_list(other_chunks)

    # Split list into chunks of five
    chapter_body_chunks = utils.chunks(other_chunks, 5)

    chapters = [{
        'title': title,
        'body_chunks': body_chunks # Shuffle order of types 
    } for title, body_chunks in itertools.zip_longest(titles, chapter_body_chunks, fillvalue=None)]


    if not os.path.exists(settings.OUTPUT_DIR):
        os.makedirs(settings.OUTPUT_DIR)

    utils.renderTemplate(env, 'book/book_template.html', os.path.join(settings.OUTPUT_DIR, 'book.html'), chapters=chapters, prompts=prompts, authors=authors.values(), paratexts=paratexts, STATIC_URL=settings.STATIC_URL)
    utils.renderTemplate(env, 'book/cover_template.html', os.path.join(settings.OUTPUT_DIR, 'cover.html'), corpus=utils.getCompleteRoundsPerType(rounds, round_types).items(), authors=authors.values(), paratexts=paratexts, STATIC_URL=settings.STATIC_URL)

    utils.renderTemplate(env, 'web/snapshot.html', os.path.join(settings.OUTPUT_DIR, 'snapshot.html'), rounds=current_author_rounds, author=current_author, STATIC_URL=settings.STATIC_URL)
    
    # Heritage index
    #latest_rounds = sorted(utils.getLatestCompletePerRoundType(rounds, round_types).values(), key=lambda r: r['start_time'])
    #utils.renderTemplate(env, 'web/index.html', os.path.join(settings.OUTPUT_DIR, 'index.html'), rounds_data=latest_rounds)
    #utils.renderTemplate(env, 'web/about.html', os.path.join(settings.OUTPUT_DIR, 'about.html'))
    
    if os.path.exists(os.path.join(settings.OUTPUT_DIR, 'static')):
        shutil.rmtree(os.path.join(settings.OUTPUT_DIR, 'static'))

    shutil.copytree(settings.STATIC_DIR, os.path.join(settings.OUTPUT_DIR, 'static'))

data = utils.loadCorpora(settings.DATA_DIR)
authors = utils.extractAuthorsFromCorpora(data)
rounds = utils.extractRoundsFromCorpora(data)

generate_html_file(rounds, authors)


