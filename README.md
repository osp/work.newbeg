Tool to generate outline fonts based on an svg skeleton.

Installation:

The tool requires Flask, installable with pip, and the Fontforge python bindings, which have to be installed with the system package manager.
